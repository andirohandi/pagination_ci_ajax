<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Belajar Pagination</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	
	
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title">Belajar Pagination</h2><br/>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-2">
						<select name='limit' id='limit' class="form-control col-sm-3 col-md-3 col" onchange='pageLoad(1)'>
							<option value='5' >5 rows</option>
							<option value='10' >10 rows</option>
							<option value='25' >25 rows</option>
						</select>
					</div>
					<div class="col-md-6"></div>
					<div class="col-md-4">
						<div class="input-group pull-right">
							<input type="text" name="cari" id='cari' class="form-control input-sm col-sm-4 col-xs-12" placeholder="Cari User . . ." onchange='pageLoad(1)'>
							<div class="input-group-btn">
								<button class="btn btn-default btn-sm"><i class="glyphicon glyphicon-search"></i></button>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<br/>
						<div id="listUser"></div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<script>
		$(document).ready(function(){
			pageLoad(1);
		});
		
		function pageLoad(i){
			var limit 	= $('#limit').val();
			var cari 	= $('#cari').val();
			
			$.ajax({
				url		: 'index.php/welcome/read/'+i,
				type	: 'post',
				dataType: 'html',
				data	: {limit:limit,cari:cari},
				beforeSend : function(){
					
				},
				success : function(result){
					$('#listUser').html(result);
				}
			})
		}
	</script>
</body>
</html>