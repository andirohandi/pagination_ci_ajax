<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');	
	}
	
	function read($pg=1){
		
		$this->load->model("model_user");
		$this->load->helper("pagination_helper");
		
		$key	= trim($this->input->post('cari',true));
		$limit	= trim($this->input->post('limit',true));
		$offset = ($limit*$pg)-$limit;
		$like	= '';
		
		if($key) $like = "(nama_user LIKE '%$key%')";
		
		$page 	= array();
        $page['limit'] 		= $limit;
        $page['count_row'] 	= $this->model_user->getCount($like);
        $page['current'] 	= $pg;
        $page['list'] 		= gen_paging($page);

        $data['paging'] = $page;
		$data['list']	= $this->model_user->getAll($like, $limit, $offset);
		
		$this->load->view('vwListUser',$data);
	}
}
